﻿using System.Collections.ObjectModel;
using System.Net.Http.Headers;
using TestApp.Models;
using TestApp.Data;
using ReactiveUI;
using System.Reactive;
using System;
using System.Threading.Tasks;
using System.Reactive.Linq;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Avalonia.Controls;
using System.Collections.Generic;
using System.Drawing;
using LiveChartsCore.SkiaSharpView;
using LiveChartsCore;
using LiveChartsCore.SkiaSharpView.Painting;
using SkiaSharp;
using LiveChartsCore.Defaults;
using System.Collections.Immutable;
using DynamicData.Aggregation;

namespace TestApp.ViewModels
{
	public class MainWindowViewModel : ViewModelBase
	{
		public MainWindowViewModel()
		{
			PortItems = new ObservableCollection<ComPort>(DataCom.GetComPorts());
			Settings = new Setting();
			dataXY = new DataXY(Settings);
			buffMinX = Settings.MinX.ToString();
			buffMaxX = Settings.MaxX.ToString();
			buffMinY = Settings.MinY.ToString();
			buffMaxY = Settings.MaxY.ToString();
			comboboxIndex = Settings.WidthLine - 1;
			CurrentPoints = new ObservableCollection<ObservablePoint>(dataXY.Points);
			Series = new ISeries[]{
				new LineSeries<ObservablePoint>
				{
					Values = CurrentPoints,
					Fill = null,
					GeometryFill = null,
					GeometryStroke = null,
					Stroke = new SolidColorPaint(SKColors.Red) { StrokeThickness = (float) 0.3 * Settings.WidthLine }
				}
			};
			StartCommand = ReactiveCommand.Create(Start);
			StopCommand = ReactiveCommand.Create(() => { StopRandom = true; });
			ResetCommand = ReactiveCommand.Create(() => { SelectedPort = null; });

			var canExecute = this.WhenAnyValue(x => x.BuffMaxX, x => x.BuffMinX, x => x.BuffMaxY, x => x.BuffMinY,
				 (maxX, minX, maxY, minY) => double.TryParse(maxX, out _) && double.TryParse(minX, out _) 
				 && double.TryParse(maxY, out _) && double.TryParse(minY, out _) 
				 && double.Parse(maxX) > double.Parse(minX) && double.Parse(maxY) > double.Parse(minY));


			SaveChangesCommamd = ReactiveCommand.Create(Saving, canExecute);
			UpdateGraphic = ReactiveCommand.Create(Update);
		}
		#region Главная вкладка
		#region Поля
		ComPort selectedPort;
		string randomNumber;
		bool stopRandom;
		bool startEnable;
		bool stopEnable;
		bool resetEnable;
		#endregion

		#region Команды
		public ReactiveCommand<Unit, Unit> StartCommand { get; }
		public ReactiveCommand<Unit, Unit> StopCommand { get; }
		public ReactiveCommand<Unit, Unit> ResetCommand { get; }
		#endregion

		#region Свойства
		public ObservableCollection<ComPort> PortItems { get; private set; }
		public ComPort SelectedPort
		{
			get => selectedPort;
			set
			{
				this.RaiseAndSetIfChanged(ref selectedPort, value);
				if (selectedPort != null)
				{
					RandomNumber = "";
					StartEnable = true;
					StopEnable = true;
					ResetEnable = true;
				}
				else
				{

					StartEnable = false;
					StopEnable = false;
					ResetEnable = false;
				}

			}
		}
		public bool StopRandom
		{
			get => stopRandom;
			set
			{
				this.RaiseAndSetIfChanged(ref stopRandom, value);
				if (stopRandom == false)
				{
					StartEnable = false;
				}
				else
				{
					StartEnable = true;
				}
			}
		}
		public string RandomNumber
		{
			get => randomNumber;
			set => this.RaiseAndSetIfChanged(ref randomNumber, value);
		}
		public bool StartEnable
		{
			set
			{
				this.RaiseAndSetIfChanged(ref startEnable, value);
			}
			get
			{
				return startEnable;
			}
		}
		public bool ResetEnable
		{
			set
			{
				this.RaiseAndSetIfChanged(ref resetEnable, value);
				if (ResetEnable == false)
				{
					StopRandom = true;
					StartEnable = false;
					RandomNumber = "";
				}
			}
			get
			{
				return resetEnable;
			}
		}
		public bool StopEnable
		{
			set
			{
				this.RaiseAndSetIfChanged(ref stopEnable, value);
			}
			get
			{
				return stopEnable;
			}
		}
		#endregion

		#region Методы
		public async void Start()
		{
			StopRandom = false;
			Random random = new Random();

			while (stopRandom == false)
			{
				RandomNumber = random.Next(0, 100).ToString();
				await Task.Delay(1000);
			}
		}
		#endregion
		#endregion

		#region Вкладка с графиком

		#region Переменные
		DataXY dataXY;
		ISeries[] series;
		ObservableCollection<ObservablePoint> currentPoints;
		#endregion

		#region Свойства
		public ISeries[] Series
		{
			get => series;
			set
			{
				this.RaiseAndSetIfChanged(ref series, value);
			}
		}
		public ObservableCollection<ObservablePoint> CurrentPoints
		{
			get => currentPoints;
			set => this.RaiseAndSetIfChanged(ref currentPoints, value);
		}
		#endregion
		
		#region Команды
		public ReactiveCommand<Unit, Unit> UpdateGraphic { get; }
		#endregion

		#region Методы
		void Update()
		{
			dataXY.GenerateNewXY(Settings);
			for (int i = 0; i < CurrentPoints.Count; i++)
			{
				CurrentPoints[i].X = dataXY.DataX[i];
				CurrentPoints[i].Y = dataXY.DataY[i];
			}
			Series = new ISeries[]{
				new LineSeries<ObservablePoint>
				{
					Values = CurrentPoints,
					Fill = null,
					GeometryFill = null,
					GeometryStroke = null,
					Stroke = new SolidColorPaint(SKColors.Red) { StrokeThickness = (float) 0.3 * Settings.WidthLine }
				}
			};
		}
		#endregion

		#endregion

		#region Вкладка с настройками

		#region Поля

		Setting settings;
		string buffMinX;
		string buffMaxX;
		string buffMinY;
		string buffMaxY;
		int selectedWidth;
		int comboboxIndex;
		bool autoSelect = false;
		bool saveChangesEnable = false;

		#endregion

		#region Свойства

		public string BuffMinX
		{
			get => buffMinX;
			set
			{
				this.RaiseAndSetIfChanged(ref buffMinX, value);
			}
		}
		public string BuffMaxX
		{
			get => buffMaxX;
			set
			{
				this.RaiseAndSetIfChanged(ref buffMaxX, value);
			}
		}
		public string BuffMinY
		{
			get => buffMinY;
			set
			{
				this.RaiseAndSetIfChanged(ref buffMinY, value);
			}
		}
		public string BuffMaxY
		{
			get => buffMaxY;
			set
			{
				this.RaiseAndSetIfChanged(ref buffMaxY, value);
			}
		}
		public int SelectedWidth
		{
			get => selectedWidth;
			set
			{
				this.RaiseAndSetIfChanged(ref selectedWidth, value);
			}
		}
		public Setting Settings
		{
			get => settings;
			set
			{
				this.RaiseAndSetIfChanged(ref settings, value);
			}
		}
		public int ComboboxIndex
		{
			get => comboboxIndex;
			set
			{
				this.RaiseAndSetIfChanged(ref comboboxIndex, value);
				switch (comboboxIndex)
				{
					case 0:
						SelectedWidth = 1;
						break;
					case 1:
						SelectedWidth = 2;
						break;
					case 2:
						SelectedWidth = 3;
						break;
				}
			}
		}

		public bool AutoSelect
		{
			get => autoSelect;
			set
			{
				this.RaiseAndSetIfChanged(ref autoSelect, value);
				BuffMaxX = Settings.AutoMaxX.ToString();
				BuffMinX = Settings.AutoMinX.ToString();
				BuffMaxY = Settings.AutoMaxY.ToString();
				BuffMinY = Settings.AutoMinY.ToString();
			}

		}
		

		#endregion

		#region Команды
		public ReactiveCommand<Unit, Unit> SaveChangesCommamd { get; }
		#endregion

		#region Методы
		void Saving()
		{
			if (BuffMaxX != null) { Settings.MaxX = Convert.ToDouble(BuffMaxX); }
			if (BuffMinX != null) { Settings.MinX = Convert.ToDouble(BuffMinX); }
			if (BuffMaxY != null) { Settings.MaxY = Convert.ToDouble(BuffMaxY); }
			if (BuffMinY != null) { Settings.MinY = Convert.ToDouble(BuffMinY); }
			Settings.WidthLine = SelectedWidth;
			Settings.Load();
		}
		#endregion

		#endregion

	}
}
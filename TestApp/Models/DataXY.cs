﻿using DynamicData;
using LiveChartsCore.Defaults;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Models
{
	public class DataXY
	{
		const int count = 1000;
		double[] dataX = new double[count];
		double[] dataY = new double[count];
		List<ObservablePoint> points = new List<ObservablePoint>(10);
		public List<ObservablePoint> Points
		{
			get => points;
			set => points = value;
		}
		public double[] DataX {
			get => dataX;
			set => dataX = value; }
		public double[] DataY
		{
			get => dataY;
			set => dataY = value;
		}

		public DataXY(Setting setting)
		{
			GenerateNewXY(setting);
		}

		public void GenerateNewXY(Setting setting)
		{
			double step = (setting.MaxX - setting.MinX) / count;
			Random rnd = new Random();
			for (int i = 0; i < DataX.Length; i++)
			{
				DataX[i] = setting.MinX + step * i;
				DataY[i] = rnd.NextDouble() * (setting.MaxY - setting.MinY) + setting.MinY;
			}
			for (int i = 0; i < count; i++)
			{
				Points.Add(new ObservablePoint(DataX[i], DataY[i]));
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TestApp.Models
{
	public class Setting
	{
		double autoMinX = 0;
		double autoMinY = 0;
		double autoMaxX = 200;
		double autoMaxY = 200;
		public double MinX { get; set; }
		public double MaxX { get; set; }
		public double MinY { get; set; }
		public double MaxY { get; set; }
		public int WidthLine { get; set; }
		public double AutoMinX => autoMinX;
		public double AutoMinY => autoMinY;
		public double AutoMaxX => autoMaxX;
		public double AutoMaxY => autoMaxY;

		public Setting()
		{
			XDocument xDocument = XDocument.Load("XMLSetting.xml");
			XElement? setting = xDocument.Element("setting");
			if (setting is not null)
			{
				XElement? Xmin = setting.Element("minX");
				XElement? Xmax = setting.Element("maxX");
				XElement? Ymin = setting.Element("minY");
				XElement? Ymax = setting.Element("maxY");
				XElement? Width = setting.Element("widthLine");

				MinX = Convert.ToDouble(Xmin?.Value);
				MaxX = Convert.ToDouble(Xmax?.Value);
				MinY = Convert.ToDouble(Ymin?.Value);
				MaxY = Convert.ToDouble(Ymax?.Value);
				WidthLine = Convert.ToInt32(Width?.Value);
			}
		}

		public void Load()
		{
			XDocument xDocument = XDocument.Load("XMLSetting.xml");

			var setting = xDocument.Element("setting");
			if (setting != null) 
			{
				var Xmin = setting.Element("minX");
				var Xmax = setting.Element("maxX");
				var Ymin = setting.Element("minY");
				var Ymax = setting.Element("maxY");
				var Width = setting.Element("widthLine");
				if (Xmin != null) Xmin.Value = MinX.ToString();
				if (Xmax != null) Xmax.Value = MaxX.ToString();
				if (Ymin != null) Ymin.Value = MinY.ToString();
				if (Ymax != null) Ymax.Value = MaxY.ToString();
				if (Width != null) Width.Value = WidthLine.ToString();
				xDocument.Save("XMLSetting.xml");
			}
		}
	}
}

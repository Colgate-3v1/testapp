﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Models
{
	public class ComPort
	{
		public string PortName { get; set; }
		public ComPort(string portName)
		{
			PortName = portName;
		}
	}
}

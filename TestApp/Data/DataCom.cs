﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Models;

namespace TestApp.Data
{
	public class DataCom
	{
		public static IEnumerable<ComPort> GetComPorts() 
		{
			var items = new List<ComPort>();
			string[] ports = SerialPort.GetPortNames();
			//string[] ports = {"COM 1", "COM 2" , "COM 3" , "COM 4" , "COM 5" };

			for (int i = 0; i < ports.Length; i++)
			{
				items.Add(new ComPort(ports[i]));
			}
			return items;
		}
	}
}
